const request = require('request');
const apiKey = 'd549a3634b9431ac0de75d96a8be81e7';

var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
  let url = `http://api.openweathermap.org/data/2.5/weather?q=Nairobi&units=metric&appid=${apiKey}`;

	request(url, function (err, response, body) {
    if(err){
      res.render('index', {weather: null, error: null});
    } else {
      let weather = JSON.parse(body);
      if(weather.main == undefined){
        res.render('index', {weather: null, error: null});
      } else {
        // let weatherText = `It's ${weather.main.temp} degrees in ${weather.name}!`;
        res.render('index', {weather: JSON.parse(body), error: null});
        // console.log(JSON.parse(body));
        // res.render('index', {weather: JSON.parse(body), error: null});
      }
    }
  });

});

module.exports = router;
